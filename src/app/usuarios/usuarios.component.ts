import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  status:any = null;
  usuarios = null;
  usuario:any = {};
  textButton:string = null;

  constructor(private fireBaseService:FirebaseService, private route:ActivatedRoute) {
      this.usuarios = fireBaseService.getUsuarios();
      this.status = this.route.snapshot.params['status'];
      this.textButton = (this.status == 'create') ? 'Guardar' : 'Actualizar';
  }
  ngOnInit() { }

  validateStatusButton(){
      if (this.status == 'create') {
          this.createUsuario();
      } else {
          this.updateUsuario();
      }
  }

  getUsuario(id){
      this.fireBaseService.getUsuario(id).valueChanges().subscribe(usuario => {
          this.usuario = usuario;
          this.textButton = 'Actualizar';
          this.status = id;
      });
  }

  cancelUsuario(){
    this.status = 'create';
    this.textButton = 'Guardar';
    this.usuario = {};
}

  createUsuario() {
      this.usuario.id = Date.now();
      this.fireBaseService.createUsuario(this.usuario);
      this.cancelUsuario();
  }

  updateUsuario(){
      this.fireBaseService.updateUsuario(this.usuario);
      this.cancelUsuario();
  }

  deleteUsuario(id){
      this.fireBaseService.deleteUsuario(id);
  }
}
