import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  constructor(private db:AngularFireDatabase) { }

  public getUsuarios() {        
      return this.db.list('Usuarios').valueChanges();
  }

  public getUsuario(id){
      return this.db.object('Usuarios/'+id);
  }

  public createUsuario(user){
      this.db.database.ref('Usuarios/'+user.id).set(user);
  }

  public updateUsuario(user){
      this.db.database.ref('Usuarios/'+user.id).set(user);
  }


  public deleteUsuario(id){
      this.db.object('Usuarios/'+id).remove();
  }
}
